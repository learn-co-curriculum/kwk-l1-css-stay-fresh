##CSS feel like a lot to remember?

Here's a great tutorial from [Shay Howe](http://learn.shayhowe.com/html-css/getting-to-know-css/) to work through to stay fresh between classes!

Also feel free to take a stab at CodeAcademy's [CSS track](http://www.codecademy.com/courses/web-beginner-en-TlhFi/0/1?curriculum_id=50579fb998b470000202dc8b)!


<p data-visibility='hidden'>KWK-L1 CSS feel like a lot to remember?</p>